import express from 'express';
import WebSocket from 'ws';

type WebSocketExtended = WebSocket & { connectionCode?: string };

export const wsController = express.Router();
const clients: Set<WebSocketExtended> = new Set();

wsController.ws('/', (ws: WebSocketExtended, req) => {
    const { connectionCode } = req.query;

    if (ws.connectionCode === undefined && connectionCode !== undefined) {
        ws.connectionCode = connectionCode;

        clients.add(ws);
    }

    ws.on('message', (msg: string) => {
        for (const client of clients) {
            if (client !== ws && client.connectionCode === connectionCode) {
                client.send(msg);
            }
        }
    });

    setInterval(() => {
        const message = {
            action: {
                type: 'KEEP_UP_WS_CONNECTION',
                payload: new Date().toTimeString(),
            },
        };

        for (const client of clients) {
            client.send(JSON.stringify(message));
        }
    }, 25000);

    ws.on('close', () => {
        clients.delete(ws);
    });
});
