import { connection } from '../db';
import { AnswerDTO } from '../interfaces';

export class AnswersModel {
    public static getAnswersByQuestionId(
        questionId: string
    ): Promise<AnswerDTO[]> {
        return new Promise((resolve, reject) => {
            connection.query(
                'SELECT * FROM answers WHERE id_question = ? ORDER BY priority',
                [questionId],
                (err, results) => {
                    if (err) {
                        reject(err);
                    }

                    resolve(results);
                }
            );
        });
    }
}
