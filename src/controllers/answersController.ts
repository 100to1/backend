import express, { Request, Response, NextFunction } from 'express';
import createError from 'http-errors';

import { AnswersModel } from '../models';

export const answersController = express.Router();

answersController.get(
    '/',
    async (req: Request, res: Response, next: NextFunction) => {
        const { questionId } = req.query;

        if (questionId === undefined) {
            next(
                createError(400, {
                    message: 'Parameter "questionId" is required',
                })
            );
        }

        try {
            const answer = await AnswersModel.getAnswersByQuestionId(
                questionId
            );

            res.json({
                data: answer,
            });
        } catch (err) {
            next(new Error(err));
        }
    }
);
