import express, { NextFunction, Request, Response } from 'express';

import { QuestionsModel } from '../models';

export const questionsController = express.Router();

questionsController.get(
    '/',
    async (req: Request, res: Response, next: NextFunction) => {
        try {
            const limit =
                req.query.limit === undefined
                    ? undefined
                    : Number(req.query.limit);
            const offset =
                req.query.offset === undefined
                    ? undefined
                    : Number(req.query.offset);
            const total = await QuestionsModel.getCount();
            const questions = await QuestionsModel.getList(limit, offset);

            res.json({
                data: questions,
                meta: {
                    total: total,
                    limit: limit ?? 0,
                    offset: offset ?? 0,
                },
            });
        } catch (err) {
            next(new Error(err));
        }
    }
);

questionsController.get(
    '/random',
    async (req: Request, res: Response, next: NextFunction) => {
        try {
            const randomQuestion = await QuestionsModel.getRandom();

            res.json({
                data: randomQuestion,
            });
        } catch (err) {
            next(new Error(err));
        }
    }
);

questionsController.get(
    '/:id',
    async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { id } = req.params;
            const question = await QuestionsModel.getById(id);

            res.json({
                data: question,
            });
        } catch (err) {
            next(new Error(err));
        }
    }
);
