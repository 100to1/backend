import { connection } from '../db';
import { QuestionDTO } from '../interfaces';

export class QuestionsModel {
    public static getList(
        limit?: number,
        offset?: number
    ): Promise<QuestionDTO[]> {
        if (limit !== undefined && offset !== undefined) {
            return new Promise((resolve, reject) => {
                connection.query(
                    'SELECT * FROM questions LIMIT ? OFFSET ?',
                    [limit, offset],
                    (err, results) => {
                        if (err) {
                            reject(err);
                        }

                        resolve(results);
                    }
                );
            });
        }

        return new Promise((resolve, reject) => {
            connection.query('SELECT * FROM questions', (err, results) => {
                if (err) {
                    reject(err);
                }

                resolve(results);
            });
        });
    }

    public static getById(id: string): Promise<QuestionDTO> {
        return new Promise((resolve, reject) => {
            connection.query(
                'SELECT * FROM questions WHERE questions.id = ?',
                [id],
                (err, results) => {
                    if (err) {
                        reject(err);
                    }

                    resolve(results[0]);
                }
            );
        });
    }

    public static getRandom(): Promise<QuestionDTO> {
        return new Promise((resolve, reject) => {
            connection.query(
                'SELECT * FROM questions ORDER BY RAND() LIMIT 1',
                (err, results) => {
                    if (err) {
                        reject(err);
                    }

                    resolve(results[0]);
                }
            );
        });
    }

    public static getCount(): Promise<number> {
        return new Promise((resolve, reject) => {
            connection.query(
                'SELECT COUNT(*) AS "count" FROM questions',
                (err, results) => {
                    if (err) {
                        reject(err);
                    }

                    resolve(results[0].count);
                }
            );
        });
    }
}
