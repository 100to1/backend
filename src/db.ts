import mysql from 'mysql';

import { MYSQL_URI } from './util/secrets';

const mysqlUri = MYSQL_URI as string;

const connection = mysql.createConnection(mysqlUri);

connection.connect(err => {
    if (err) {
        throw err;
    }
});

export { connection };
