import express, { Request, Response, NextFunction } from 'express';
import expressWs from 'express-ws';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import createError from 'http-errors';
import cors from 'cors';

// Create Express server
const app = express();

// Express configuration
app.set('port', process.env.PORT ?? 3000);
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
expressWs(app);

import {
    questionsController,
    answersController,
    wsController,
} from './controllers';

// routers
app.use('/', wsController);
app.use('/questions', questionsController);
app.use('/answers', answersController);

// catch 404 and forward to error handler
app.use((req: Request, res: Response, next: NextFunction) => {
    next(createError(404));
});

export default app;
