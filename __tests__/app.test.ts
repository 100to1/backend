import request from 'supertest';

import app from '../src/app';

describe('GET /questions', () => {
    it('responds with json', async () => {
        const res = await request(app).get('/questions');

        expect(res.status).toEqual(200);
        expect(res.type).toEqual('application/json');
    });

    it('has meta property with expected object', async () => {
        const res = await request(app).get('/questions');
        const meta = { total: 689 };

        expect(res.body.meta).toMatchObject(meta);
    });
});

describe('GET /questions/1', () => {
    it('responds with json', async () => {
        const res = await request(app).get('/questions/1');

        expect(res.status).toEqual(200);
        expect(res.type).toEqual('application/json');
    });

    it('has body with expected object', async () => {
        const res = await request(app).get('/questions/1');
        const body = {
            data: {
                id: 1,
                name: 'кто это?-грозно вопрошает муж. что отвечает жена?',
            },
        };

        expect(res.body).toMatchObject(body);
    });
});

describe('GET /questions/random', () => {
    it('responds with json', async () => {
        const res = await request(app).get('/questions/random');

        expect(res.status).toEqual(200);
        expect(res.type).toEqual('application/json');
        expect(typeof res.text).toEqual('string');
    });
});

describe('GET /answers', () => {
    it('responds with html and 400 status', async () => {
        const res = await request(app).get('/answers');

        expect(res.status).toEqual(400);
        expect(res.type).toEqual('text/html');
    });

    it('responds with json and 200 status', async () => {
        const res = await request(app).get('/answers?questionId=1');

        expect(res.status).toEqual(200);
        expect(res.type).toEqual('application/json');
        expect(typeof res.text).toEqual('string');
    });
});
