import { QuestionDTO } from './QuestionDTO';

export interface AnswerDTO {
    id: number;
    name: string;
    id_question: QuestionDTO['id'];
    priority: number;
}
